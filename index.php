<?php
class mosca
{

        private $name;
        public function __construct($nid)
        {
		$nid=isset($nid)?$nid:"default";
                $this->name = "/var/lock/mosca-".$nid.".lock";
                $this->ttl  = 1800; # mitja hora
        }
        public function crea()
        {
                $f_id=fopen($this->name,'w+');
                fwrite($f_id,posix_getpid());
                fclose($f_id);
        }

        // retorna cert si podem entrar, fals si encara tenim funcionant l'altre
        public function check()
        {
                # afegim codi de queue.php
                $inici = 0;
                $final = 24;
                $hora = date("H") * 1;
                # si no esta dintre de les hores correctes sortim
                if(!($hora >= $inici && $hora < $final)) { return false; }

                //Comprovem si ja s'està executant
                if( file_exists($this->name) )
                {
                        $mod=filemtime($this->name);

                        if(time()-$mod <= $this->ttl )
                        {
                                //Considerem que el procés pre-existent encara va
                                return false; #die;
                        }

                        //A partir d'aqui considerem que el procés que esta en marxa, esta mort o congelat u_u
                        $pid=@file_get_contents($this->name);
                        exec("kill $pid");
                        @unlink($this->name);
                }

                # retornem cert, ja que o no existeix el fitxer
                # i per tant no hi ha teoricament cap reindexador funcionant
                # o acabem de matar i unlinkar en fitxer de PID. 
                return true;
        }

        public function mata()
        {
                if(file_exists($this->name)) $ok = unlink($this->name);
                else return true;
                if(!$ok) exec("rm -f $this->name");
                $ok = !file_exists($this->name);
        }
}

?>

